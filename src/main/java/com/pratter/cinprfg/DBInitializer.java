package com.pratter.cinprfg;

import java.time.LocalDate;
import java.util.List;

import com.pratter.cinprfg.domain.Car;
import com.pratter.cinprfg.persistence.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

@Configuration
public class DBInitializer {
    @Autowired
    private CarRepository carRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void handleApplicationEvent() {
        carRepository.saveAll(
                List.of(
                        new Car("Cadillac", "Escalade", LocalDate.of(2018, 3, 1)),
                        new Car("Genesis", "G90", LocalDate.of(2019, 9, 1))
                )
        );

        System.out.println(carRepository.findAll());
    }
}
