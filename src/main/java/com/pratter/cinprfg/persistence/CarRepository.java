package com.pratter.cinprfg.persistence;

import java.util.List;

import com.pratter.cinprfg.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}