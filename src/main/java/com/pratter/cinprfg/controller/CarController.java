package com.pratter.cinprfg.controller;

import com.pratter.cinprfg.domain.Car;
import com.pratter.cinprfg.persistence.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// see http://localhost:8081/resources/persons

@RestController
@RequestMapping("/resources/cars")
public class CarController {
    @Autowired
    private CarRepository carRepository;

    @GetMapping
    public List<Car> retrieveAll() {
        return carRepository.findAll();
    }

}
