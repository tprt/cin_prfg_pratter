package com.pratter.cinprfg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PratterCinPrfgApplication {

    public static void main(String[] args) {
        SpringApplication.run(PratterCinPrfgApplication.class, args);
    }

}
